# Fedora Install Guide

The Fedora Install Guide has been deprecated and removed in F37.

The latest published version is in the `f36` branch.
